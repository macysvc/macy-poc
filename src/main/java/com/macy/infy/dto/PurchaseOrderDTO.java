package com.macy.infy.dto;

public class PurchaseOrderDTO {

	private String purchaseOrderNumber;
	private String purchaseOrderDate;
	private String vendorName;
	private String contactName;
	private String trackingNumber;

	
	/*public PurchaseOrderDTO(String purchaseOrderNumber, String purchaseOrderDate, String vendorName, String contactName,
			String trackingNumber) {
		super();
		this.purchaseOrderNumber = purchaseOrderNumber;
		this.purchaseOrderDate = purchaseOrderDate;
		this.vendorName = vendorName;
		this.contactName = contactName;
		this.trackingNumber = trackingNumber;
	}*/
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	
	public String getPurchaseOrderDate() {
		return purchaseOrderDate;
	}
	public void setPurchaseOrderDate(String purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	
	
	
}
