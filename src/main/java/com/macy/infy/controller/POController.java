package com.macy.infy.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.macy.infy.dto.PurchaseOrderDTO;
import com.macy.infy.service.POService;

/**
 * @author madhumathi.v
 *
 */
@RestController
public class POController {
	
	@Autowired
	private POService poService;
	
	
	@RequestMapping("/purchaseOrder/getAllPurchaseOrder")
	public List<PurchaseOrderDTO> getAllPurchaseOrder() {
		
		return poService.getAllPurchaseOrder();		
		
	}
	

	@RequestMapping("/purchaseOrder/{poNumber}")
	public PurchaseOrderDTO getPurchaseOrder(@PathVariable String poNumber) {
		
		return poService.getPurchaseOrder(poNumber);		
		
	}
	
	@PostMapping("/purchaseOrder/insertNewRowOrder")
	public String insertPurchaseOrder(@RequestParam String po_order_num,
			@RequestParam String po_order_date,
			@RequestParam String vendor_name,
			@RequestParam String contact_name,
			@RequestParam String tracking_num) {
				return poService.insertValuestoTable(po_order_num,po_order_date,vendor_name,contact_name,tracking_num);
		
	}
	
}
