package com.macy.infy.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.BigqueryScopes;
import com.google.api.services.bigquery.model.GetQueryResultsResponse;
import com.google.api.services.bigquery.model.QueryRequest;
import com.google.api.services.bigquery.model.QueryResponse;
import com.google.api.services.bigquery.model.TableCell;
import com.google.api.services.bigquery.model.TableRow;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryError;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.TableId;
import com.macy.infy.dto.PurchaseOrderDTO;

@Service
public class POService {

	/*
	 * @Autowired private PurchaseOrderRepository purchaseOrderRepository;
	 */

	/*
	 * private List<PurchaseOrder> poList = Arrays.asList(
	 * 
	 * new PurchaseOrder("12345", "1st January2018", "abc", "debojit",
	 * "12345abcde"), new PurchaseOrder("67890", "5th January2018", "xyz", "ajay",
	 * "67890vwxyz")
	 * 
	 * );
	 */

	private static final Logger logger = Logger.getLogger(POService.class.getName());
	
	
	public List<PurchaseOrderDTO> getAllPurchaseOrder(){

		String projectId = "my-project-infy-1015";
		List<PurchaseOrderDTO> poList = new ArrayList<PurchaseOrderDTO>();
		try {
			// Create a new Bigquery client authorized via Application Default Credentials.
			Bigquery bigquery = getAuthorizedClient();

			List<TableRow> rows;
			rows = executeQuery("SELECT * FROM [my-project-infy-1015.MacysDataset.PurchaseOrderMacy] LIMIT 1000",
					bigquery, projectId);
			int count = 0;
			for (TableRow row : rows) {
				PurchaseOrderDTO po = new PurchaseOrderDTO();
				for (TableCell field : row.getF()) {

					System.out.println(field.getV());
					if (count == 0) {
						po.setPurchaseOrderNumber(field.getV().toString());
					} else if (count == 1) {
						po.setPurchaseOrderDate(field.getV().toString());
					} else if (count == 2) {
						po.setVendorName(field.getV().toString());
					} else if (count == 3) {
						po.setContactName(field.getV().toString());
					} else if (count == 4) {
						po.setTrackingNumber(field.getV().toString());
					}
					count++;
				}
				System.out.println();
				poList.add(po);
				count = 0;

			}
			
			if(poList.size() == 0)
				logger.info("No purchase order found");
			
			return poList; 

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.severe("IOException in executing code -" + e.getMessage());
			e.printStackTrace();
			return null;
		}catch (Exception e) {
			// TODO Auto-generated catch block
			logger.severe("Exception in executing code -" + e.getMessage());
			e.printStackTrace();
			return null;
		}		
		
	
	}
	

	public PurchaseOrderDTO getPurchaseOrder(String poNumber) {

		if ("".equals(poNumber) || poNumber == null) {

			logger.info("POService - Purchase Order Number is empty or blank");
			logger.severe("POService - Purchase Order Number is empty or blank");
			return null;
		}

		String projectId = "my-project-infy-1015";
		List<PurchaseOrderDTO> poList = new ArrayList<PurchaseOrderDTO>();
		try {
			// Create a new Bigquery client authorized via Application Default Credentials.
			Bigquery bigquery = getAuthorizedClient();

			List<TableRow> rows;
			rows = executeQuery("SELECT * FROM [my-project-infy-1015.MacysDataset.PurchaseOrderMacy] WHERE purchaseOrderNumber = '"+poNumber+"' LIMIT 1000",
					bigquery, projectId);
			int count = 0;
			for (TableRow row : rows) {
				PurchaseOrderDTO po = new PurchaseOrderDTO();
				for (TableCell field : row.getF()) {

					System.out.println(field.getV());
					if (count == 0) {
						po.setPurchaseOrderNumber(field.getV().toString());
					} else if (count == 1) {
						po.setPurchaseOrderDate(field.getV().toString());
					} else if (count == 2) {
						po.setVendorName(field.getV().toString());
					} else if (count == 3) {
						po.setContactName(field.getV().toString());
					} else if (count == 4) {
						po.setTrackingNumber(field.getV().toString());
					}
					count++;
				}
				System.out.println();
				poList.add(po);
				count = 0;

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.severe("Exception in executing code -" + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("poList=" + poList);

		try {
			/*return poList.stream().filter(po -> po.getPurchaseOrderNumber().equals(poNumber)).findFirst().get();*/
			return poList.get(0); 
		} catch (Exception e) {
			logger.severe("No such purchase order found");
			throw new RuntimeException("Purchase order mismatch");
		}
	}

	/**
	 * Executes the given query synchronously.
	 */
	private static List<TableRow> executeQuery(String querySql, Bigquery bigquery, String projectId)
			throws IOException {
		QueryResponse query = bigquery.jobs().query(projectId, new QueryRequest().setQuery(querySql)).execute();

		// Execute it
		GetQueryResultsResponse queryResult = bigquery.jobs()
				.getQueryResults(query.getJobReference().getProjectId(), query.getJobReference().getJobId()).execute();

		return queryResult.getRows();
	}

	public Bigquery getAuthorizedClient() throws IOException {

		// Create the credential
		HttpTransport transport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		GoogleCredential credential = GoogleCredential
				.fromStream(new FileInputStream("src/main/resources/creds/MyProjectInfy1015-96ed3b0e2041.json"));
		

		if (credential.createScopedRequired()) {
			credential = credential.createScoped(BigqueryScopes.all());
		}
		
		return new Bigquery.Builder(transport, jsonFactory, credential).setApplicationName("Bigquery Samples").build();

	}

	public String insertValuestoTable(String po_order_num, String po_order_date, String vendor_name,
			String contact_name, String tracking_num) {
		System.out.println("Trying inserting" + po_order_num + " " + po_order_date + " " + vendor_name + " "
				+ contact_name + " " + tracking_num);
		logger.info("Inserting" + po_order_num + " " + po_order_date + " " + vendor_name + " " + contact_name + " "
				+ tracking_num);

		BigQuery bigQuery = getBigQueryClient();
		TableId tableId = TableId.of("MacysDataset", "PurchaseOrderMacy");
		// Values of the row to insert
		Map<String, Object> rowContent = new HashMap<>();
		rowContent.put("purchaseOrderNumber", po_order_num);
		rowContent.put("purchaseOrderDate", po_order_date);
		rowContent.put("vendorName", vendor_name);
		rowContent.put("contactName", contact_name);
		rowContent.put("trackingNumber", tracking_num);
		
		InsertAllResponse response = bigQuery
				.insertAll(InsertAllRequest.newBuilder(tableId).addRow(rowContent).build());// .addRow("rowId",
																							// rowContent)
		// More rows can be added in the same RPC by invoking .addRow() on the builder

		if (response.hasErrors()) {
			// If any of the insertions failed, this lets you inspect the errors
			for (Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
				// inspect row error
			}
			logger.severe("Failed to insert row " + po_order_num + " " + po_order_date + " " + vendor_name + " "
					+ contact_name + " " + tracking_num);
			return "Failed to insert row" + po_order_num + " " + po_order_date + " " + vendor_name + " " + contact_name
					+ " " + tracking_num;

		} else {
			logger.info("Success in inserting row " + po_order_num + " " + po_order_date + " " + vendor_name + " "
					+ contact_name + " " + tracking_num);
			return "Success in inserting row" + po_order_num + " " + po_order_date + " " + vendor_name + " "
					+ contact_name + " " + tracking_num;

		}

	}

	private BigQuery getBigQueryClient() {
		BigQuery bigQuery = null;
		try {
			String projectId = "my-project-infy-1015";
			final File credentialsPath = new File("src/main/resources/creds/MyProjectInfy1015-96ed3b0e2041.json");
			FileInputStream serviceAccountStream = new FileInputStream(credentialsPath);
			System.out.println("Starting....1");
			GoogleCredentials credentials;
			credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
			System.out.println("Starting....2");
			bigQuery = BigQueryOptions.newBuilder().setProjectId(projectId).setCredentials(credentials).build()
					.getService();
			return bigQuery;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bigQuery;
	}

}
