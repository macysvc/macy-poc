package com.macy.infy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author debojit.banerjee
 *
 */
@SpringBootApplication
public class WorkloadApp {

	public static void main(String[] args) {
		
		SpringApplication.run(WorkloadApp.class, args);

	}

}
